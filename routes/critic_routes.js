var express = require('express');
var router = express.Router();
var critic_dal = require('../model/critic_dal');


router.get('/all', function(req, res) {
    critic_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('critic/criticViewAll', { 'result':result });
        }
    });

});

// View the company for the given id
router.get('/', function(req, res){
    if(req.query.critic_name == null) {
        res.send('critic_name is null');
    }
    else {
        critic_dal.getById(req.query.critic_name, function(err,result) {
            if (err) {
                res.send(err);
            }
            else {
                res.render('critic/criticViewById', {'result': result});
            }
        });
    }
});


router.get('/add', function(req, res){
    // passing all the query parameters (req.query) to the insert function instead of each individually
    critic_dal.getAll(function(err,result) {
        if (err) {
            res.send(err);
        }
        else {
            res.render('critic/criticAdd', {'critic': result});
        }
    });
});




router.get('/insert', function(req, res){
    // simple validation
    if(req.query.critic_name == "") {
        res.send('Please provide a critic name.');
    }


    else {
        // passing all the query parameters (req.query) to the insert function instead of each individually
        critic_dal.insert(req.query, function(err,result) {
            if (err) {
                console.log(err)
                res.send(err);
            }
            else {
                critic_dal.getAll(function(err, result){
                    if(err) {
                        res.send(err);
                    }
                    else {
                        res.render('critic/criticViewAll', { 'result':result, was_successful: true });
                    }
                });
            }
        });
    }
});

router.get('/edit', function(req, res){
    if(req.query.critic_name == null) {
        res.send('A critic name is required.');
    }
    else {
        critic_dal.edit(req.query.critic_name, function(err, result){
            res.render('critic/criticUpdate', {critic: result[0]});
        });
    }
});



router.get('/update', function(req, res) {
    critic_dal.update(req.query, function(err, result){
        res.redirect(302, '/critic/all');
    });
});


router.get('/delete', function(req, res){
    if(req.query.critic_name == null) {
        res.send('critic_name is null');
    }
    else {
        critic_dal.delete(req.query.critic_name, function(err, result){
            if(err) {
                res.send(err);
            }
            else {
                //poor practice, but we will handle it differently once we start using Ajax
                res.redirect(302, '/critic/all');
            }
        });
    }
});

module.exports = router;
