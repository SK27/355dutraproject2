var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.insert = function(params, callback) {

    // insert the studio
    var query = 'INSERT INTO studio (studio_name, country, studio) VALUES (?)';

    var queryData = [params.studio_name, params.country, params.studio];

    connection.query(query, [queryData], function(err, result) {
        callback(err, result);
    });
};


exports.getAll = function(callback) {
    var query = 'SELECT * FROM studio';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getById = function(studio_name, callback) {
    var query = 'SELECT * FROM studio WHERE studio_name = ?';
    var queryData = [studio_name];
    console.log(query);

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });
};


exports.update = function(params, callback) {
    var query = 'UPDATE studio SET studio_name = ?, country = ?, employess = ?,  WHERE studio_name = ?';
    var queryData = [params.studio_name, params.country, params.studio,params.studio_name];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};

exports.delete = function(studio_name, callback) {
    var query = 'DELETE FROM studio WHERE studio_name = ?';
    var queryData = [studio_name];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};


exports.edit = function(studio_name, callback) {
    var query = 'SELECT * FROM studio WHERE studio_name = ?';
    var queryData = [studio_name];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};